﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Day_7.Model;

namespace Day_7.Events
{
   public class AccountService
    {

        private void Credit(string account, int amount, string senderAccount)
        {
            Console.WriteLine("Credit Alert!");
            Console.WriteLine("Your account has been Credited");
            Console.WriteLine($"Amount: {amount}");
            Console.WriteLine($"Account: {account}");
            Console.WriteLine($"From: {senderAccount}");
            Console.WriteLine($"Time: {DateTime.Now}");
        }


        private void Debit(string account, int amount)
        {
            Console.WriteLine("Debit Alert!");
            Console.WriteLine("Your account has been Debit");
            Console.WriteLine($"Amount: {amount}");
            Console.WriteLine($"Account: {account}");
            Console.WriteLine($"Time: {DateTime.Now}");
        }

        public void RecordBalance(object sender, BalanceCheckViewModel model)
        {
            Console.WriteLine("-------Saving----");
        }
        public void RecordTransfer(object sender, TransferViewModel model)
        {
            Credit(model.DestAccount, model.Amount, model.SenderAccount);
            Debit(model.SenderAccount, model.Amount);
        }

        public void RecordWithdrawal(object sender, WithdrawalViewModel model)
        {
            Debit(model.Account, model.Amount);
        }
    }
}
